import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_archiver_appliance_service_running_and_enabled(host):
    service = host.service('archiver-appliance')
    assert service.is_running
    assert service.is_enabled


def test_tomcat_services_running_and_enabled(host):
    for instance in ('mgmt', 'engine', 'etl', 'retrieval'):
        service = host.service('tomcat@' + instance)
        assert service.is_running
        assert service.is_enabled


def test_tomcat_instances_access(host):
    # There is no database server available so the appliance won't start properly
    cmd = host.run('curl -L http://localhost:17665/mgmt/ui/index.html')
    assert 'Service Unavailable' in cmd.stdout


def test_site_policies(host):
    site_policies = host.file("/var/lib/tomcats/site_policies.py")
    assert site_policies.exists
    assert site_policies.contains("def determinePolicy(")


def test_appliances_xml(host):
    appliances_xml = host.file("/var/lib/tomcats/appliances.xml")
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-epicsarchiverap-default":
        assert appliances_xml.contains("<data_retrieval_url>http://")
        assert appliances_xml.contains(":17668/retrieval</data_retrieval_url>")
    else:
        assert appliances_xml.contains("<data_retrieval_url>http://appliance.example.org/retrieval</data_retrieval_url>")


def test_logrotate_config(host):
    config = host.file("/etc/logrotate.d/archiver-appliance")
    assert config.contains("/var/log/tomcat/archappl-policy.log")
