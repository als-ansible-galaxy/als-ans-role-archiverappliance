ics-ans-role-epicsarchiverap
============================

Ansible role to install [EPICS Archiver Appliance](http://slacmshankar.github.io/epicsarchiver_docs/index.html) on CentOS 7.

This role doesn't take care of creating the database and required tables.
You should use the ics-ans-role-mariadb for that. See the ics-ans-archappl playbook.

Note that this role doesn't take care of firewall rules.
You should use another role to configure the firewall or disable it.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
# EPICS Archiver Appliance release and url
epicsarchiverap_release: "0.0.1_SNAPSHOT_15-November-2018T10-27-25"
epicsarchiverap_release_tag: "v0.0.1_SNAPSHOT_15-Nov-2018"
epicsarchiverap_release_name: "archappl_v{{ epicsarchiverap_release }}"
epicsarchiverap_url: "https://artifactory.esss.lu.se/artifactory/swi-pkg/archiver-appliance/{{ epicsarchiverap_release_tag }}/{{ epicsarchiverap_release_name }}.tar.gz"

# root directory where the archiver appliance release will be unarchived
epicsarchiverap_release_root: /opt
# root location of the short, medium and long term stores
epicsarchiverap_storage: /home/archappl

# mysql/mariadb parameters
epicsarchiverap_db_name: archappl
epicsarchiverap_db_user: archappl
epicsarchiverap_db_password: archappl
epicsarchiverap_db_url: mysql://localhost:3306

# To deploy a cluster, this variable shall be set to the Ansible group
# that defines the instances of the cluster
# Shall be undefined by default for single appliance
# epicsarchiverap_cluster: aa_cluster

# To use a load balancer in front of the cluster for data retrieval,
# set the following variable:
# epicsarchiverap_data_retrieval_url: http://archiver.example.org/retrieval

# Tomcat instances HTTP connector port
epicsarchiverap_mgmt_port: 17665
epicsarchiverap_engine_port: 17666
epicsarchiverap_etl_port: 17667
epicsarchiverap_retrieval_port: 17668

# root directory where tomcat instances are installed
epicsarchiverap_tomcats_base: "/var/lib/tomcats"

# JAVA options
epicsarchiverap_java_maxmetaspace: "256M"
epicsarchiverap_java_initial_heapsize: "512M"
epicsarchiverap_java_max_heapsize: "512M"

# EPICS variables
# EPICS_CA_ADDR_LIST will only be defined if this variable is not empty
epicsarchiverap_epics_ca_addr_list: ""
epicsarchiverap_epics_ca_auto_addr_list: "yes"
# Set EPICS_CA_MAX_ARRAY_BYTES to 2**24 (16Mb)
epicsarchiverap_epics_ca_max_array_bytes: 16777216

# Site policies
epicsarchiverap_site_policies_url: "https://gitlab.esss.lu.se/ics-infrastructure/epicsarchiver-config/raw/master/policies/ess_policies.py"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epicsarchiverap
```

License
-------

BSD 2-clause

The following templates come from the official Apache Tomcat installation and are included under the [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0.txt):

- templates/context.xml.j2
- templates/server.xml.j2
- templates/tomcat-users.xml.j2
- templates/web.xml.j2

A local copy of the licenses can be found in the files LICENSE and Apache_2.0_License.txt.
